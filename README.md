# ics-ans-role-sis8300drv

Ansible role to install the kernel module driver for Struck sis8300 DAQ card.
This role depends on the kernel-rt role.

## Role Variables

```yaml
sis8300drv_rpm_version: 4.8.0-0
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-sis8300drv
```

## License

BSD 2-clause
