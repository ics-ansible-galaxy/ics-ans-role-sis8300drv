*********************************
Vagrant driver installation guide
*********************************

Requirements
============

* Vagrant
* Virtualbox
* vagrant-yocto

Install
=======

.. code-block:: bash

    $ vagrant plugin install vagrant-yocto --plugin-source https://artifactory.esss.lu.se/artifactory/api/gems/ics-vagrant-plugin/
